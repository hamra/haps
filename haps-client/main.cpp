/* main.cpp -- Main file for the software */

/* Copyright (C) 2018 Waleed Hamra

   This file is part of HAPS, Hamra Administrator Password Solution

   HAPS is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   HAPS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with HAPS.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QCoreApplication>
#include <QTimer>

#include "mainclass.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    MainClass myMain;

    QObject::connect(&myMain, SIGNAL(finished()),
             &a, SLOT(quit()));

    QTimer::singleShot(10, &myMain, SLOT(run()));

    return a.exec();
}
