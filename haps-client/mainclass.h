/* mainclass.h -- Main header file for the main class */

/* Copyright (C) 2018 Waleed Hamra

   This file is part of HAPS, Hamra Administrator Password Solution

   HAPS is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   HAPS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with HAPS.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAINCLASS_H
#define MAINCLASS_H

#include <QObject>
#include <QCoreApplication>
#include <QStringList>
#include <QtNetwork/QTcpSocket>
#include <QDebug>
#include <QtNetwork/QHostAddress>
#include <QString>
#include <QProcess>
#include <QSslSocket>

class MainClass : public QObject
{
    Q_OBJECT
public:
    explicit MainClass(QObject *parent = nullptr);
    void quit();

signals:
    void finished();

public slots:
    void run();

private:
    QCoreApplication *app;
    QHostAddress serverIp;
    int port;
    QSslSocket *socket;
    QString readData(QSslSocket *socket);
};

#endif // MAINCLASS_H
