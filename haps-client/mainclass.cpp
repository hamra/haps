/* mainclass.cpp -- Main implementation file for the main class */

/* Copyright (C) 2018 Waleed Hamra

   This file is part of HAPS, Hamra Administrator Password Solution

   HAPS is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   HAPS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with HAPS.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainclass.h"

MainClass::MainClass(QObject *parent)
    : QObject(parent)
{
    app = QCoreApplication::instance();
}

void MainClass::quit()
{
    if (socket->isOpen())
    {
        socket->disconnectFromHost();
        delete socket;
    }
    emit finished();
}

void MainClass::run()
{
    QStringList arguments = app->arguments();
    if (arguments.size() < 3)
    {
        qDebug() << "Missing argument";
        quit();
        return;
    }
    if (!serverIp.setAddress(arguments.at(1)))
    {
        qDebug() << "IP Address not understood";
        quit();
        return;
    }
    port = arguments.at(2).toUInt();
    if (port > 65535 || port < 1 )
    {
        qDebug() << "Invalid Port";
        quit();
        return;
    }
    socket = new QSslSocket();
    socket->setPeerVerifyMode(QSslSocket::VerifyNone);
    socket->connectToHostEncrypted(serverIp.toString(), port);
    if (!readData(socket).startsWith("hello", Qt::CaseInsensitive))
    {
        qDebug() << "Communication failed";
        quit();
        return;
    }
    QString computerName = QSysInfo::machineHostName();
    socket->write("check " + computerName.toLatin1() +"\r\n");
    QString answer = readData(socket).trimmed();
    QString newPassword;
    if (answer.startsWith("good", Qt::CaseInsensitive))
    {
        quit();
        return;
    }
    else if (answer.startsWith("new", Qt::CaseInsensitive))
    {
        newPassword = answer.simplified().split(QRegExp("\\s+")).at(1).trimmed();
    }
    else
    {
        qDebug() << "Didn't understand server response";
        quit();
        return;
    }
    QProcess *netUser = new QProcess(this);
    netUser->start("C:\\Windows\\System32\\net.exe user Administrator " + newPassword);
    if (netUser->state() == QProcess::Running)
        netUser->waitForFinished(10000);
    qDebug() << "Exit code: " << netUser->exitCode();
    if (netUser->exitCode() != 0)
    {
        socket->write("fail\r\n");
        socket->flush();
        quit();
        return;
    }
    else
    {
        qDebug() << "Number of bytes written at end: " << socket->write("ok \r\n");
    }
    delete netUser;
    socket->flush();
    quit();
    return;
}

QString MainClass::readData(QSslSocket *socket)
{
    QString buffer;
    while (!buffer.contains("\n") && socket->waitForReadyRead(30000))
    {
        buffer = buffer + QString(socket->readAll());

    }
    if (!buffer.contains("\n"))
    {
        qDebug() << "No data arrived or incomplete";
        return QString("");
    }
    return buffer.trimmed();
}
