/* tcpserver.cpp -- Functions to start server and listen for connections */

/* Copyright (C) 2018 Waleed Hamra

   This file is part of HAPS, Hamra Administrator Password Solution

   HAPS is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   HAPS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with HAPS.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "tcpserver.h"

tcpServer::tcpServer(keepass *pwManager, QObject *parent)
    :QTcpServer(parent), pwManager(pwManager)
{

}

void tcpServer::incomingConnection(qintptr socketDescriptor)
{
    clientThread *thread = new clientThread(socketDescriptor, pwManager, this);
    connect (thread, SIGNAL(sysMessage(QString,QtServiceBase::MessageType)), this, SIGNAL(sysMessage(QString,QtServiceBase::MessageType)));
    connect (thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    thread->start();
}
