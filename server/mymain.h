/* mymain.h -- Main header file for the main class */

/* Copyright (C) 2018 Waleed Hamra

   This file is part of HAPS, Hamra Administrator Password Solution

   HAPS is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   HAPS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with HAPS.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MYMAIN_H
#define MYMAIN_H

#include <QObject>
#include <QSettings>
#include <QDebug>
#include <QCoreApplication>
#include <QtNetwork/QTcpServer>
#include <QDir>
#include <QObject>

#include "keepass.h"
#include "tcpserver.h"
#include <QtService>

class myMain : public QObject, public QtService<QCoreApplication>
{
    Q_OBJECT
public:
    explicit myMain(int argc, char **argv);

public slots:
    void sendMessage(QString message, QtServiceBase::MessageType msgType);

signals:
    void finished();


private:
    QCoreApplication *app;
    keepass *pwManager;
    QTcpServer *server;

protected:
    void start();
    void stop();
    void pause();
    void resume();

};

#endif // MYMAIN_H
