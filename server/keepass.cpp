/* keepass.cpp -- Functions to interact with KeePass 2 */

/* Copyright (C) 2018 Waleed Hamra

   This file is part of HAPS, Hamra Administrator Password Solution

   HAPS is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   HAPS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with HAPS.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "keepass.h"

keepass::keepass(string p_filePath, string p_kpspacePath, string p_filePassword, string p_fileCopy)
    : filePath(p_filePath), kpspacePath(p_kpspacePath), filePassword(p_filePassword), fileCopy(p_fileCopy)
{
    qDebug() << "Constructing keepass, filePath=" << filePath.c_str() << " kpspacePath=" << kpspacePath.c_str();
    state = wtf;
    if (!fileExists(filePath))
    {
        state = fileNotFound;
        return;
    }
    if (!fileExists(kpspacePath))
    {
        state = pluginNotFound;
        return;
    }

#ifdef _WIN32
    program = "\"" + kpspacePath + "\"";
#elif __linux__
    program = exec("which cli").at(0) + " " + kpspacePath;
#else
#error
#endif

    if (!checkPw())
    {
        state = wrongPw;
        return;
    }
    state = good;
    qDebug() << "keepass constructed. state: " << state;
}

bool keepass::fileExists(string p_filePath)
{
    ifstream ifile(p_filePath.c_str());
    return bool(ifile);
}

vector<string> keepass::exec(const char *cmd)
{
    qDebug() << "Entering exec as: " << cmd;
    array<char, 128> buffer;
    string result;
    vector<string> v;
    shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if (!pipe)
    {
        v.push_back("Command failed!");
        return v;
    }
    while (!feof(pipe.get()))
    {
        if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
            result += buffer.data();
    }
    istringstream stream(result);
    while (!stream.eof())
    {
        string s;
        getline(stream, s);
        v.push_back(s);
    }
    return v;
}

vector<string> keepass::kpspace(string parameters)
{
    qDebug() << "entering kpspace as: " << parameters.c_str();
    return exec(("\"" + program + " " + parameters + "\"").c_str());
}

bool keepass::checkPw()
{
    qDebug() << "entering checkPw";
    string parameters = " -c:ListGroups " + filePath + " -pw:" + filePassword;
    vector<string> result = kpspace(parameters);
    return checkSuccess(result);
}

bool keepass::addEntry(string computerName, string password, string group)
{
    lock.lockForWrite();
    time_t tt = chrono::system_clock::to_time_t(chrono::system_clock::now() + days{90});
    stringstream ss;
    ss << put_time(localtime(&tt), "%Y-%m-%d");
    string expiryDate = ss.str();
    cout << expiryDate << endl;
    string parameters = " -c:AddEntry \"" + filePath + "\" -pw:" + filePassword + " -GroupName:\"" + group + "\" -Title:" \
            + computerName + " -UserName:" + computerName + " -Password:" + password + " -setx-ExpiryTime:" + expiryDate + " -setx-Expires:true ";
    cout << parameters << endl;
    bool value = checkSuccess(kpspace(parameters));
    if (value)
        copy();
    lock.unlock();
    return value;
}

bool keepass::editEntry(string computerName, string password, string group)
{
    lock.lockForWrite();
    time_t tt = chrono::system_clock::to_time_t(chrono::system_clock::now() + days{90});
    stringstream ss;
    ss << put_time(localtime(&tt), "%Y-%m-%d");
    string expiryDate = ss.str();
    string parameters = " -c:EditEntry \"" + filePath + "\" -pw:" + filePassword + " -refx-Group:\"" + group + "\" -ref-Title:" \
            + computerName + " -set-Password:" + password + " -setx-ExpiryTime:" + expiryDate + " -setx-Expires:true ";
    bool value = checkSuccess(kpspace(parameters));
    if (value)
        copy();
    lock.unlock();
    return value;
}

keepass::entryStatus keepass::checkEntryStatus(string computerName, string group)
{
    qDebug() << "entering checkEntryStatus with Computer=" << computerName.c_str() << " and group=" << group.c_str();
    lock.lockForRead();
    string parameters1 = " -c:GetEntryString \"" + filePath + "\" -pw:" + filePassword + " -refx-Group:\"" + group + "\" -ref-Title:" \
            + computerName + " -Field:UserName -FailIfNoEntry -refx-Expired:false";
    string parameters2 = " -c:GetEntryString \"" + filePath + "\" -pw:" + filePassword + " -refx-Group:\"" + group + "\" -ref-Title:" \
            + computerName + " -Field:UserName -FailIfNoEntry -refx-Expired:true";

    if (checkSuccess(kpspace(parameters1)))
    {
        lock.unlock();
        return valid;
    }
    else if (checkSuccess(kpspace(parameters2)))
    {
        lock.unlock();
        return expired;
    }
    else
    {
        lock.unlock();
        return notThere;
    }
}

string keepass::genPw()
{
    return kpspace(" -c:GenPw").at(0);
}

bool keepass::deleteEntry(string computerName, string group)
{
    lock.lockForWrite();
    string parameters = "-c:MoveEntry \"" + filePath + "\" -pw:" + filePassword + " -refx-Group:\"" + group + "\" -ref-Title:" \
            + computerName + " -GroupName:\"Recycle Bin\" ";
    bool value = checkSuccess(kpspace(parameters));
    if (value)
        copy();
    lock.unlock();
    return value;
}

bool keepass::checkSuccess(vector<string> result)
{
    for (unsigned int i = 0; i < result.size(); i++)
    {
        if (regex_search(result.at(i), regex("^OK:")))
        {
            return true;
        }
        else if (regex_search(result.at(i), regex("^E:")))
        {
            cerr << result.at(i) << endl;
            return false;
        }
    }
    cerr << "Not sure if command succeeded or failed, considering it a failure!" << endl;
    return false;
}

void keepass::copy()
{
    ifstream src(filePath, ios::binary);
    ofstream dst(fileCopy, ios::binary);

    dst << src.rdbuf();
}
