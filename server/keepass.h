/* keepass.h -- Header for keepass integration */

/* Copyright (C) 2018 Waleed Hamra

   This file is part of HAPS, Hamra Administrator Password Solution

   HAPS is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   HAPS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with HAPS.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KEEPASS_H
#define KEEPASS_H

#include <string>
#include <fstream>
#include <iostream>
#include <cstdio>
#include <memory>
#include <stdexcept>
#include <array>
#include <vector>
#include <sstream>
#include <regex>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <QObject>
#include <QReadWriteLock>
#include <QDebug>

using namespace std;
using days = std::chrono::duration
    <int, std::ratio_multiply<std::ratio<24>, std::chrono::hours::period>>;

class keepass : public QObject
{
    Q_OBJECT
public:
    keepass(string p_filePath, string p_kpspacePath, string p_filePassword, string p_fileCopy);
    enum KPStatus {good, fileNotFound, pluginNotFound, wrongPw, fileLocked, wtf};
    enum entryStatus {valid, expired, notThere};
    KPStatus state;
    bool addEntry(string computerName, string password, string group = "Computers");
    bool editEntry(string computerName, string password, string group = "Computers");
    entryStatus checkEntryStatus(string computerName, string group = "Computers");
    string genPw();
    bool deleteEntry(string computerName, string group = "Computers");

private:
    bool fileExists(string p_filePath);
    string filePath;
    string kpspacePath;
    string filePassword;
    string fileCopy;
    string program;
    vector<string> exec(const char* cmd);
    vector<string> kpspace(string parameters);
    bool checkPw();
    bool checkSuccess(vector<string> result);
    QReadWriteLock lock;
    void copy();
};

#endif // KEEPASS_H
