/* mymain.cpp -- Main class launching server and handling services */

/* Copyright (C) 2018 Waleed Hamra

   This file is part of HAPS, Hamra Administrator Password Solution

   HAPS is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   HAPS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with HAPS.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mymain.h"

myMain::myMain(int argc, char **argv)
    : QtService<QCoreApplication>(argc, argv, "HAPS Server")
{
    setServiceDescription("HAPS Server to organize passwords");
    setServiceFlags(QtServiceBase::CanBeSuspended);
}

void myMain::sendMessage(QString message, QtServiceBase::MessageType msgType)
{
    logMessage(message, msgType);
}

void myMain::stop()
{
    app->quit();
}

void myMain::pause()
{
    server->pauseAccepting();
}

void myMain::resume()
{
    server->resumeAccepting();
}

void myMain::start()
{
    QCoreApplication *app = application();
    qRegisterMetaType<QtServiceBase::MessageType>("QtServiceBase::MessageType");
    QString currentDir = app->applicationDirPath();
    QSettings *settings = new QSettings(currentDir + "/serverconfig.ini", QSettings::IniFormat);
    if (settings->value("KeepassFile").isValid() && settings->value("KpscriptFile").isValid() && settings->value("KeepassPw").isValid() && settings->value("fileCopy").isValid())
    {
        pwManager = new keepass(settings->value("KeepassFile").toString().toStdString(), settings->value("KpscriptFile").toString().toStdString(),
                                settings->value("KeepassPw").toString().toStdString(), settings->value("fileCopy").toString().toStdString());
        switch (pwManager->state)
        {
        case keepass::wtf:
        case keepass::fileNotFound:
        case keepass::pluginNotFound:
        case keepass::fileLocked:
        case keepass::wrongPw:
            qDebug() << "Keepass Object failed!";
            qDebug() << "Keepass Object state: " << pwManager->state;
            app->quit();
            break;
        case keepass::good:
            qDebug() << "Keepass Object succeeded!";
            break;
        default:
            qDebug() << "Wait, what?";
            app->quit();
            break;
        }
    }
    else
    {
        qDebug() << "Configuration file is incomplete!";
        qDebug() << settings->value("KeepassFile");
        qDebug() << settings->value("KpscriptFile");
        qDebug() << settings->value("KeepassPw");
        logMessage("Configuration file is incomplete!", QtServiceBase::Error);
        app->quit();
    }


    server = new tcpServer(pwManager);
    if (!server->listen(QHostAddress::Any, 3400))
    {
        qDebug() << "failed to start server!";
        logMessage("Failed to start server", QtServiceBase::Error);
        app->quit();
    }
    logMessage("HAPS server started successfully", QtServiceBase::Success);
    connect(server, SIGNAL(sysMessage(QString,QtServiceBase::MessageType)), this, SLOT(sendMessage(QString,QtServiceBase::MessageType)));
}

