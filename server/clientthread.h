/* clientthread.h -- Header for clientThread class */

/* Copyright (C) 2018 Waleed Hamra

   This file is part of HAPS, Hamra Administrator Password Solution

   HAPS is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   HAPS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with HAPS.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CLIENTTHREAD_H
#define CLIENTTHREAD_H

#include <QThread>
#include <QTcpSocket>
#include <QStringList>
#include <QString>
#include <QtService>
#include <QSslSocket>
#include <QSettings>
#include <QCoreApplication>
#include <QFile>
#include <QSslKey>
#include <QSslCertificate>

#include "keepass.h"

class clientThread : public QThread
{
    Q_OBJECT
public:
    clientThread(int socketDescriptor, keepass *pwManager, QObject *parent);
    ~clientThread();
    void run() override;

private:
    int socketDescriptor;
    keepass *pwManager;
    QString readData(QTcpSocket *socket);
    QString computerName;
    QString password;
    QStringList buffer;
    QSslSocket *socket;

signals:
    void error(QTcpSocket::SocketError socketError);
    void sysMessage(QString message, QtServiceBase::MessageType msgType);
};

#endif // CLIENTTHREAD_H
