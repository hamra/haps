/* clientthread.cpp -- Thread handling client communication */

/* Copyright (C) 2018 Waleed Hamra

   This file is part of HAPS, Hamra Administrator Password Solution

   HAPS is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   HAPS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with HAPS.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "clientthread.h"

clientThread::clientThread(int socketDescriptor, keepass *pwManager, QObject *parent)
    : QThread(parent), socketDescriptor(socketDescriptor), pwManager(pwManager)
{

}

clientThread::~clientThread()
{
    delete socket;
}

void clientThread::run()
{
    qDebug() << "entering clientThread::run()";
    qDebug() << "This thread: " << this->thread();
    qDebug() << "Current Thread: " << currentThread();
    QSettings *settings = new QSettings(QCoreApplication::applicationDirPath() + "/serverconfig.ini", QSettings::IniFormat);
    socket = new QSslSocket();
    if (!socket->setSocketDescriptor(socketDescriptor))
    {
        qDebug() << "Socket Error!";
        emit error(socket->error());
        return;
    }
    QFile sslCert(settings->value("sslCert").toString());
    if (!sslCert.open(QIODevice::ReadOnly))
    {
        qDebug() << "Failed to open Cert file: " << settings->value("sslCert").toString();
        emit sysMessage("Server failed to open certificate file", QtServiceBase::Error);
        return;
    }
    socket->setLocalCertificate(QSslCertificate(sslCert.readAll()));
    QFile sslKey(settings->value("sslKey").toString());
    if (!sslKey.open(QIODevice::ReadOnly))
    {
        qDebug() << "Failed to open key file";
        emit sysMessage("Server failed to open key file", QtServiceBase::Error);
        return;
    }
    socket->setPrivateKey(QSslKey(sslKey.readAll(), QSsl::Rsa));
    socket->startServerEncryption();
    socket->waitForEncrypted();
    socket->write("Hello\r\n");
    buffer = readData(socket).split(QRegExp("\\s+"));
    if (buffer.size() != 2 || buffer.at(0) != "check")
    {
        qDebug() << "Weird reply from client" << buffer;
        socket->disconnectFromHost();
        socket->waitForDisconnected();
        return;
    }
    computerName = buffer.at(1).trimmed();
    keepass::entryStatus status = pwManager->checkEntryStatus(computerName.toStdString());
    if (status == keepass::valid)
    {
        socket->write("good\r\n");
        socket->disconnectFromHost();
        socket->waitForDisconnected();
        return;
    }
    else if (status == keepass::expired)
    {
        password = QString::fromStdString(pwManager->genPw());
        if (!pwManager->deleteEntry(computerName.toStdString()))
        {
            qDebug() << "Editing Password failed!";
            socket->write("fail\r\n");
            socket->disconnectFromHost();
            socket->waitForDisconnected();
            return;
        }
        if (!pwManager->addEntry(computerName.toStdString(), password.toStdString()))
        {
            qDebug() << "Editing Password failed!";
            socket->write("fail\r\n");
            socket->disconnectFromHost();
            socket->waitForDisconnected();
            return;
        }
        socket->write("new " + password.toLatin1() + "\r\n");
        if (!readData(socket).startsWith("ok", Qt::CaseInsensitive))
        {
            pwManager->deleteEntry(computerName.toStdString());
            qDebug() << "Client has trouble";
        }
        socket->disconnectFromHost();
        socket->waitForDisconnected();
        return;
    }
    else if (status == keepass::notThere)
    {
        password = QString::fromStdString(pwManager->genPw());
        if (!pwManager->addEntry(computerName.toStdString(), password.toStdString()))
        {
            qDebug() << "Adding entry failed!";
            socket->write("fail\r\n");
            socket->disconnectFromHost();
            socket->waitForDisconnected();
            return;
        }
        socket->write("new " + password.toLatin1() + "\r\n");
        if (!readData(socket).startsWith("ok", Qt::CaseInsensitive))
        {
            emit sysMessage("Computer " + computerName + " failed setting the password", QtServiceBase::Warning);
            pwManager->deleteEntry(computerName.toStdString());
            qDebug() << "Client has trouble";
        }
        else
            emit sysMessage("Computer " + computerName + " changed its password successfully", QtServiceBase::Success);
        socket->disconnectFromHost();
        socket->waitForDisconnected();
        return;
    }
    else
        qDebug() << "Get out of here!";
}

QString clientThread::readData(QTcpSocket *socket)
{
    QString buffer;
    while (!buffer.contains("\n") && socket->waitForReadyRead(30000))
    {
        buffer = buffer + QString(socket->readAll());

    }
    if (!buffer.contains("\n"))
    {
        qDebug() << "No data arrived or incomplete, data:" << buffer;
        return QString("");
    }
    return buffer.trimmed();
}
